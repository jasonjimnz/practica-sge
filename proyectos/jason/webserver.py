# -*- coding: utf-8 -*-
from flask import Flask, request, session, g, redirect, url_for, abort, \
    render_template, escape
# from multiprocessing.pool import ThreadPool

from htmlParser import Parser

from model import Model, check_password

app = Flask(__name__)

m = Model('mysql', 'database')

p = Parser('<p>Hola Mundo</p>')



def get_user_by_cookie():
    response = None
    if 'session_token' in session:
        session_token = m.get_user_session_by_token(session['session_token'])
        if session_token:
            response = session_token.user
            print "has session"
    return response


def get_default_context():
    context = {
        'msj': 'Hello world',
        'user': get_user_by_cookie()
    }
    return context

def generate_modal(modal_id, msj, head = None, footer = None):
    return {
        'id':modal_id,
        'msj':msj,
        'head': head,
        'footer': footer
    }


@app.route('/', methods=['GET', 'POST'])
def index():
    context = get_default_context()
    print session
    return render_template('index.html', context=context)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET' and 'username' not in session:
        context = get_default_context()
        return render_template('login.html', context=context)
    elif request.method == 'POST' and 'username' not in session:
        u = request.form['email']
        p = request.form['auth']
        user = m.get_user_by_email(u)
        if user and user.active:
            response = check_password(user.password, p)
            print("Usuario")
            print response
            if response:
                user_session = m.create_user_session(user)
                session['username'] = u
                session['session_token'] = user_session
            else:
                pass
                # TODO: Manage login errors
        elif user and not user.active:
            pass
        # TODO: Manage Non-activated users
        else:
            pass
            # TODO: Manage non-existing user login attempt
    return redirect('/')


@app.route('/logout', methods=['GET'])
def logout():
    if 'session_token' in session:
        user_session = m.get_user_session_by_token(session['session_token'])
    else:
        user_session = None
    if user_session:
        m.delete_sqlalchemy_entity(user_session)
        m.session.close()
    session.pop('username', None)
    session.pop('session_token', None)
    return redirect(url_for('login'))

@app.route('/register',methods=['GET','POST'])
def register():
    if 'session_token' in session:
        return redirect('index')

    if request.method == 'GET':
        context = get_default_context()
        return render_template('register.html', context=context)
    elif request.method == 'POST':
        u = request.form['user']
        e = request.form['email']
        p = request.form['pass']

        user = m.create_user(e,u,p)
        user = m.update_user(user, active=True)
    return redirect('login')

# BLOG VIEWS

@app.route('/blog', methods=['GET'])
def blogView():
    context = get_default_context()
    context['categories'] = []
    for c in m.get_categories():
        context['categories'].append(
                {
                    'category': c,
                    'entries': []
                }
            )
    return render_template('blog_main.html', context=context)

@app.route('/blog/category/add', methods=['POST'])
def blogAddCategory():
    n = request.form['name']
    s = request.form['slug']
    d = request.form['description']
    c = m.create_category(n, s, d)
    m.session.close()
    return redirect(url_for('blogView'))

@app.route('/blog/category/entries/<slug>', methods=['GET'])
def blogViewCategory(slug):
    c = m.find_category_by_slug(slug)
    if c:
        context = get_default_context()
        context['category'] = c
        context['entries'] = m.find_entries_by_category(c)
        return render_template('blog_category.html', context=context)
    else:
        return redirect(url_for('blogView'))

@app.route('/blog/category/<slug>/entry/add' ,methods=['GEt', 'POST'])
def blogViewAddEntry(slug):
    category = m.find_category_by_slug(slug)
    if request.method == 'GET':
        context = get_default_context()
        context['category'] = category
        return render_template('components/article_form.html',context=context)
    if category:
        title = request.form['title']
        slug = request.form['slug']
        content = request.form['content']
        try:
            if request.form['draft']:
                draft = True
            else:
                draft = False
        except Exception as e:
            draft = False

        try:
            description = request.form['description']
        except Exception as e:
            description = None

        try:
            keywords = request.form['keywords']
        except Exception as e:
            keywords = None
        entry = m.create_entry(
            title,
            slug,
            content,
            category,
            draft,
            description,
            keywords,
        )
        m.session.close()
    return redirect(url_for('blogViewCategory', slug=category.slug))

@app.route('/blog/entry/<slug>', methods=['GET'])
def viewEntry(slug):
    e = m.find_entry_by_slug(slug)
    modal = generate_modal(
        e.slug,
        e.title,
        e.content
        )
    return render_template('components/modal_articles.html',modal = modal)

@app.route('/error')
def defaultError():
    modal =generate_modal(
        'error',
        'Se ha producido un error',
        'Hubo un error en el sistema intentalo en otro momento'
        )
    return render_template('components/modal.html', modal = modal)

SECRET_KEY = "ASDFASDFASDFA"
app.secret_key = SECRET_KEY

if __name__ == "__main__":
    print(m.get_user_by_email('jasonjimenezcruz@gmail.com').email)
    app.run(host="0.0.0.0", debug=True)
